+++
title = "Anchor links working using ox-hugo"
author = ["Kaushal Modi"]
date = 2018-08-27T00:00:00-04:00
draft = false
+++

## What is the issue {#what-is-the-issue}

If I create a link to a local heading [a header link](#another-heading), the browser renders the link relative to the webroot. If you click on the link it will navigate the browser to the webroot and query for the anchor from that location rather than the current document. The same issue happens when tring to link to a footnote&nbsp;[^fn:1].

**Also you do not require a blank line between headings.**


## Another heading {#another-heading}

-   Link to [What is the issue](#what-is-the-issue) using heading string.
-   Link to [What is the issue](#what-is-the-issue) using CUSTOM\_ID.

[^fn:1]: this is a footnote.
